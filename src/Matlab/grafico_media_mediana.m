
%ylim([-99 -0]);
%xlim([0 30]);

%xmedia = 0:0.5:15;
x = linspace(0,15);
xmedia = [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 11.5 12 12.5 13 13.5 14 14.5 15];
ymedia = [-47.84375 -62.093750 -61.81250 -72.90625 -76.37500 -74.90625 -75.375000 -75.93750 -77.125000 -82.562500 -80.50000 -81.34375 -79.21875 -79.28125 -78.78125 -82.25000 -82.40625 -84.31250 -84.09375 -85.31250 -85.62500 -83.40625 -83 -80.7187500 -82.37500 -81.21875 -79.93750 -84.21875 -85.09375 -82.12500 -84.18750]; 
figure(1)
plot(xmedia,ymedia,'-r.','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('M�dia da coleta de dados � esquerda do m�dulo iTag','FontSize',12);

p=3/10;
g=xmediana.^p;
k = sum(ymediana.*g)/sum(g.*g);
xc = linspace(min(xmediana),max(xmediana),100);
yc = k*(xc.^p);
figure(5)
plot(xc,yc,'g','LineWidth',2);
xticks(0:1:15)
clc;

x = linspace(0,15);
%x = 0:0.5:15;
xmediana = [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 11.5 12 12.5 13 13.5 14 14.5 15];
ymediana = [-49 -62 -60 -71 -76 -74 -73 -75.5 -76.5 -82 -79.5 -81 -78 -78.5 -77 -82 -82 -84.5 -84 -87 -87 -83.5 -85 -83 -86 -86 -83 -84 -86 -81 -86]; 
figure(2)
plot(xmediana,ymediana,'-b.','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('Mediana da coleta de dados � esquerda do m�dulo iTag','FontSize',12);

p=3/10;
g=xmediana.^p;
k = sum(ymediana.*g)/sum(g.*g);
xc = linspace(min(xmediana),max(xmediana),100);
yc = k*(xc.^p);
figure(6)
plot(xc,yc,'g','LineWidth',2);
xticks(0:1:15)
%ylim([-99 -0]);
%xlim([0 30]);

clc;

x = linspace(0,15);
%x = 0:0.5:15;
xmedia = [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 11.5 12 12.5 13 13.5 14 14.5 15];
ymedia = [-70.46875 -67.4375 -69.25000 -76.25000 -77.34375 -80.75000 -80.84375 -81 -78.15625 -82.28125 -81.15625 -84.87500 -81.59375 -83.96875 -84.43750 -82.28125 -85.18750 -85.21875 -87.09375 -84.87500 -83.40625 -83 -84.21875 -82.37500 -80.71875 -84.18850 -82.37500 -84.09375 -85.09375 -82.38400 -83.5293]; 
figure(3)
plot(xmedia,ymedia,'-r.','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('M�dia da coleta de dados � direita do m�dulo iTag','FontSize',12);

p=3/10;
g=xmediana.^p;
k = sum(ymediana.*g)/sum(g.*g);
xc = linspace(min(xmediana),max(xmediana),100);
yc = k*(xc.^p);
figure(7)
plot(xc,yc,'g','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('M�todo dos m�nimos quadrados aplicado aos dados coletados','FontSize',12);
clc;

x = linspace(0,15);
%x = 0:0.5:15;
xmediana = [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10 10.5 11 11.5 12 12.5 13 13.5 14 14.5 15];
ymediana = [-71 -68 -70 -76 -76 -81 -81 -80.5 -78 -82 -81 -84.5 -82 -84 -84 -82 -83.5 -85 -87 -85 -84.5 -81.5 -86 -83 -81 -86 -85 -84 -86 -84 -84.5]; 
figure(4)
plot(xmediana,ymediana,'-b.','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('Mediana da coleta de dados � direita do m�dulo iTag','FontSize',12);

%m�nimos quadrados - y = k*(x^p)
p=3/10;
g=xmediana.^p;
k = sum(ymediana.*g)/sum(g.*g);
xc = linspace(min(xmediana),max(xmediana),100);
yc = k*(xc.^p);
figure(8)
plot(xc,yc,'g','LineWidth',2);
xticks(0:1:15)
xlabel('dist�ncia (m)','FontSize',13);
ylabel('pot�ncia do sinal (dBm)','FontSize',13);
title('M�todo dos m�nimos quadrados aplicado aos dados coletados','FontSize',12);