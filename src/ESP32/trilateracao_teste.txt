#include <BLEDevice.h>
 
#define ADDRESS "fc:58:fa:44:06:01" //Endereço do iTag, conseguido pelo próprio scan pretp
#define ADDRESS2 "fc:58:fa:44:0c:2e" //branco
#define ADDRESS3 "fc:58:fa:43:f0:3e" //azul
#define LED_2 2 //Pino do led1
#define LED_13 13  //Pino do led2
#define LED_12 12  //Pino do led3

#define SCAN_INTERVAL 5 //intervalo entre cada scan
#define TARGET_RSSI -70 //RSSI limite para ligar o led.
#define MAX_MISSING_TIME 10 //Tempo para desligar o relê desde o momento que o iTag não for encontrado
 
BLEScan* pBLEScan; //Variável que irá guardar o scan
uint32_t lastScanTime = 0; //Quando ocorreu o último scan

int found[3] ={0,0,0};

int rssi_vetor[3] ={0,0,0};
boolean ok = false;
//boolean found = false; //Se encontrou o iTag no último scan
//boolean found2 = false; //Se encontrou o iTag no último scan
//boolean found3 = false; //Se encontrou o iTag no último scan
uint32_t lastFoundTime = 0; //Tempo em que encontrou o iTag pela última vez
int rssi = 0;
int rssi2 = 0;
int rssi3 = 0;

//Callback das chamadas ao scan
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks
{
    void onResult(BLEAdvertisedDevice advertisedDevice)
    {
        //Sempre que um dispositivo for encontrado ele é mostrado aqui
        //Serial.print("Device found: ");      
        //Serial.println(advertisedDevice.toString().c_str());
 
        //Se for o dispositivo que esperamos
        if(advertisedDevice.getAddress().toString() == ADDRESS)
        {
            //Marcamos como encontrado, paramos o scan e guardamos o rssi
            found[0] = 1;
           // advertisedDevice.getScan()->stop(); //para o scan
            rssi = advertisedDevice.getRSSI();

        }
        if(advertisedDevice.getAddress().toString() == ADDRESS2)
        {
            //Marcamos como encontrado, paramos o scan e guardamos o rssi
            found[1] = 1;
          //  advertisedDevice.getScan()->stop();
            rssi2 = advertisedDevice.getRSSI();

        }
        if(advertisedDevice.getAddress().toString() == ADDRESS3)
        {
            //Marcamos como encontrado, paramos o scan e guardamos o rssi
            found[2] = 1;
           // advertisedDevice.getScan()->stop();
            rssi3 = advertisedDevice.getRSSI();

        }
        if(found[0] == 1 && found[1] == 1 && found[2] == 1)
        {
            Serial.println(rssi);
            //Serial.println("preto");
            Serial.println(rssi2);
            //Serial.println("branco");
            Serial.println(rssi3);
            //Serial.println("azul");
        advertisedDevice.getScan()->stop();
      rssi_vetor[0] = rssi; //preto, branco e azul
      rssi_vetor[1] = rssi2;
      rssi_vetor[2] = rssi3;
      found[0] = 0;
      found[1] = 0;
      found[2] = 0;
      ok = true;
        }

    }
};

void setup()
{
    Serial.begin(115200);
    //Configura o pino do relê como saída e coloca com low
    pinMode(LED_2, OUTPUT);
    digitalWrite(LED_2, LOW);
    pinMode(LED_13, OUTPUT);
    digitalWrite(LED_13, LOW);
    pinMode(LED_12, OUTPUT);
    digitalWrite(LED_12, LOW);
 
    //Guardamos a referência e configuramos o objeto scan
    BLEDevice::init(""); 
    pBLEScan = BLEDevice::getScan();
    pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
    pBLEScan->setActiveScan(true);
}

void loop()
{   
    uint32_t now = millis(); //Tempo em milissegundos desde o boot
    uint32_t now2 = millis(); //Tempo em milissegundos desde o boot
    uint32_t now3 = millis(); //Tempo em milissegundos desde o boot

 if(ok)
 {
      lastFoundTime = millis(); //Tempo em milissegundos de quando encontrou
       if(rssi_vetor[0] > -79 && rssi_vetor[1] <-72 && rssi_vetor[2] > -85 )
       {
      digitalWrite(LED_2, HIGH);
      digitalWrite(LED_13, LOW);
      delay(100);
            rssi_vetor[0] = 0; //preto, branco e azul
      rssi_vetor[1] = 0;
      rssi_vetor[2] = 0;
      ok = false;
       }
 
    else if (rssi_vetor[0] <= -79 && rssi_vetor[1] >= -75 && rssi_vetor[2] >= -85)
    {
      digitalWrite(LED_13, HIGH);
      digitalWrite(LED_2, LOW);
      delay(100);
      rssi_vetor[0] = 0; //preto, branco e azul
      rssi_vetor[1] = 0;
      rssi_vetor[2] = 0;
      ok = false;
    }
    else
    {
      digitalWrite(LED_13, LOW);
      digitalWrite(LED_2, LOW);
      rssi_vetor[0] = 0; //preto, branco e azul
      rssi_vetor[1] = 0;
      rssi_vetor[2] = 0;
      ok = false;
    }
  /*  if(found && found2 && found3){ //Se econtrou o iTag no último scan
       lastFoundTime = millis(); //Tempo em milissegundos de quando encontrou
        found = false;
        found2 = false;
        found3 = false;
      digitalWrite(LED_2, HIGH);
    }
    else
    {
      digitalWrite(LED_2, LOW);
      }
    */   
 }
 
    if(now - lastScanTime > SCAN_INTERVAL || now2 - lastScanTime > SCAN_INTERVAL || now3 - lastScanTime > SCAN_INTERVAL){ //Se está no tempo de fazer scan
        //Marca quando ocorreu o último scan e começa o scan
        lastScanTime = now;
        pBLEScan->start(1);
        
    }
}